############################
#
# Anomaly Events
#
# Written by Miranda van den Brink
#
############################

namespace = anomaly

@tier1influencecontact = 7
@tier1influencecontactxenophile = 8
@tier1influencecontactmin = 20
@tier1influencecontactmax = 80

@tier2influencecontact = 10
@tier2influencecontactxenophile = 12
@tier2influencecontactmin = 40
@tier2influencecontactmax = 100

@tier1materialreward = 6
@tier1materialmin = 100
@tier1materialmax = 500
@tier2materialreward = 12
@tier2materialmin = 150
@tier2materialmax = 1000
@tier3materialreward = 18
@tier3materialmin = 250
@tier3materialmax = 1500
@tier4materialreward = 24
@tier4materialmin = 350
@tier4materialmax = 2000
@tier5materialreward = 48
@tier5materialmin = 700
@tier5materialmax = 4000

@tier1influencereward = 6
@tier1influencemin = 40
@tier1influencemax = 100
@tier2influencereward = 12
@tier2influencemin = 80
@tier2influencemax = 175
@tier3influencereward = 18
@tier3influencemin = 125
@tier3influencemax = 250
@tier4influencereward = 24
@tier4influencemin = 150
@tier4influencemax = 300
@tier5influencereward = 36
@tier5influencemin = 250
@tier5influencemax = 500
@tier6influencereward = 48
@tier6influencemin = 300
@tier6influencemax = 600

@tier1researchreward = 6
@tier1researchmin = 60
@tier1researchmax = 150
@tier2researchreward = 12
@tier2researchmin = 90
@tier2researchmax = 250
@tier3researchreward = 18
@tier3researchmin = 120
@tier3researchmax = 350
@tier4researchreward = 24
@tier4researchmin = 150
@tier4researchmax = 500
@tier5researchreward = 48
@tier5researchmin = 300
@tier5researchmax = 1000

## ROOT = ship scope
## FROM = planet scope

# Limbo - from anomaly_events_2
ship_event = {
	id = anomaly.185
	title = "anomaly.185.name"
	desc = "anomaly.185.desc"
	show_sound = event_radio_chatter
	picture = GFX_evt_archaeological_dig
	location = from

	is_triggered_only = yes

	option = {
		name = OK
		from = {
			enable_special_project = {
				name = "LIMBO_1_PROJECT"
				location = this
				owner = root
			}
		}
	}
}

# Limbo project 1 followup - anomaly_events_2
ship_event = {
	id = anomaly.186
	title = "anomaly.186.name"
	desc = {
		text = anomaly.186.desc.v1
		trigger = { 
			hidden:owner = {
				OR = {
					has_policy_flag = ai_outlawed
					NOR = {
						has_technology = tech_robotic_workers
						has_technology = tech_droid_workers
						has_technology = tech_synthetic_workers 
					}
				}
			}
		}
	}

	desc = {
		text = anomaly.186.desc.v2
		trigger = { 
			hidden:owner = { 
				has_technology = tech_robotic_workers 
				NOR = { 
					has_technology = tech_droid_workers
					has_technology = tech_synthetic_workers
					has_policy_flag = ai_outlawed
				}
			}
		}
	}

	desc = {
		text = anomaly.186.desc.v3
		trigger = {
			hidden:owner = { 
				has_technology = tech_synthetic_workers
				NOT = {
					has_policy_flag = ai_outlawed
				}
			}
		}
	}
	picture = GFX_evt_sentient_AI
	show_sound = event_radio_chatter
	location = fromfrom
	is_triggered_only = yes

	option = {
		name = "anomaly.186.a"
		hidden_effect = { owner = { country_event = { id = story.5 days = 30 } } }
		owner = {
			add_monthly_resource_mult = {
				resource = engineering_research
				value = @tier4researchreward
				min = @tier4researchmin
				max = @tier4researchmax
			}
		}
	}

	option = {
		name = "anomaly.186.b"
		trigger = {
			owner = {
					has_technology = tech_robotic_workers
				NOR = {	
					has_technology = tech_droid_workers
					has_technology = tech_synthetic_workers
					has_policy_flag = ai_outlawed
				}
			}
		}	
		hidden_effect = {
			fromfrom = { set_planet_flag = limbo_planet }
			owner = {
				set_country_flag = limbo_downloaded
			}
		}
	}

	#option = {
	#	name = "anomaly.186.c"
	#	trigger = {
	#		owner = {
	#				has_technology = tech_robotic_workers
	#			NOR = {	
	#				has_technology = tech_droid_workers
	#				has_technology = tech_synthetic_workers
	#				has_policy_flag = ai_outlawed
	#			}
	#		}
	#	}
	#	owner = {
	#		capital_scope = {
	#			enable_special_project = {
	#				name = "LIMBO_2_PROJECT"
	#				location = this
	#				owner = root.owner
	#			}
	#		}
	#	}
	#}

	option = {
		name = "anomaly.186.d"
		trigger = {
			owner = {
				OR = {
					NOR = {	
						has_technology = tech_robotic_workers
						has_technology = tech_droid_workers
						has_technology = tech_synthetic_workers
					}
					has_policy_flag = ai_outlawed
				}
			}
		}
		owner = {
			add_monthly_resource_mult = {
				resource = engineering_research
				value = @tier3researchreward
				min = @tier3researchmin
				max = @tier3researchmax
			}
		}
		hidden_effect = {
			fromfrom = { set_planet_flag = limbo_planet }
			owner = {
				set_country_flag = limbo_downloaded
			}
		}
	}

	#option = {
	#	name = "anomaly.186.e"
	#	trigger = {
	#		owner = {
	#			has_technology = tech_robotic_workers
	#			has_technology = tech_droid_workers
	#			NOT = {	has_policy_flag = ai_outlawed }
	#		}
	#	}
	#	owner = {
	#		capital_scope = {
	#			enable_special_project = {
	#				name = "LIMBO_2_PROJECT"
	#				location = this
	#				owner = root.owner
	#			}
	#		}
	#    }
	#}       
}

#country_event = {
#	id = anomaly.187
#	title = "anomaly.187.name"
#	desc = "anomaly.187.desc"
#	picture = GFX_evt_sentient_AI
#	show_sound = event_radio_chatter
#	location = FROM
#
#	is_triggered_only = yes
#
#	option = {
#		name = anomaly.187.a
#		trigger = {
#			has_technology = tech_robotic_workers
#			any_owned_planet = { free_pop_tiles > 0 }
#			NOT = {	
#				#has_technology = tech_droid_workers
#				#has_technology = tech_synthetic_workers
#				has_policy_flag = ai_outlawed
#			}
#		}
#		random_owned_planet = {
#			limit = { free_pop_tiles > 0 }
#			best_tile_for_pop = {
#				build_pop = {
#    				name = buildable_robot_pop_1
#    				grown = yes
#    			}
#    		}
#    	}
#	}
#
#	#Failsafe option: dismantle the robots for parts
#	option = {
#		name = anomaly.189.c
#		custom_tooltip = anomaly.186.a.alt.tooltip
#		add_minerals = 1500 
#	}
#}
#
#country_event = {
#	id = anomaly.188
#	title = "anomaly.188.name"
#	desc = "anomaly.188.desc"
#	picture = GFX_evt_sentient_AI
#	show_sound = event_radio_chatter
#	location = from
#
#	is_triggered_only = yes
#
#	option = {
#		name = OK
#		add_monthly_resource_mult = {
#			resource = engineering_research
#			value = @tier3researchreward
#			min = @tier3researchmin
#			max = @tier3researchmax
#		}
#		add_monthly_resource_mult = {
#			resource = society_research
#			value = @tier3researchreward
#			min = @tier3researchmin
#			max = @tier3researchmax
#		}
#	}
#}
#
## The droids/synths leave and make a colony, inside or outside of the player's borders
#country_event = {
#	id = anomaly.189
#	title = "anomaly.189.name"
#	desc = "anomaly.189.desc"
#	picture = GFX_evt_sentient_AI
#	show_sound = event_radio_chatter
#	location = FROM
#
#	is_triggered_only = yes
#
#	# build a colony within our borders option
#	option = {
#		name = anomaly.189.a
#		#custom_tooltip = anomaly.189.a.tooltip
#		trigger = {
#			NOT = { has_policy_flag = ai_outlawed }
#			any_planet_within_border = {
#				habitable_planet = yes
#				is_colony = no 
#				has_anomaly = no
#				is_capital = no
#			}
#		}
#		allow = {
#			NOT = { has_policy_flag = ai_outlawed }
#			any_planet_within_border = {
#				habitable_planet = yes
#				is_colony = no 
#				has_anomaly = no
#				is_capital = no
#			}
#		}
#		hidden_effect = {
#			random_planet_within_border = {
#				limit = {
#					habitable_planet = yes
#					is_colony = no	
#					has_anomaly = no
#					is_capital = no
#				}
#				set_owner = root
#				set_controller = root
#				prevent_anomaly = yes 
#				
#				best_tile_for_pop = {
#					set_building = "building_colony_shelter"
#				}
#				best_tile_for_pop = {
#					build_pop = {
#    					name = buildable_robot_pop_3
#    					grown = yes
#    					effect = {
#    						if = {
#							    limit = { root = { has_ethic = ethic_fanatic_xenophile } }
#							    pop_add_ethic = ethic_fanatic_xenophile
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_collectivist } }
#							    pop_add_ethic = ethic_fanatic_collectivist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_individualist } }
#							    pop_add_ethic = ethic_fanatic_individualist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_xenophobe } }
#							    pop_add_ethic = ethic_fanatic_xenophobe
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_militarist } }
#							    pop_add_ethic = ethic_fanatic_militarist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_pacifist } }
#							    pop_add_ethic = ethic_fanatic_pacifist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_spiritualist } }
#							    pop_add_ethic = ethic_fanatic_spiritualist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_materialist } }
#							    pop_add_ethic = ethic_fanatic_materialist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_xenophile } }
#							    pop_add_ethic = ethic_xenophile
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_collectivist } }
#							    pop_add_ethic = ethic_collectivist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_individualist } }
#							    pop_add_ethic = ethic_individualist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_xenophobe } }
#							    pop_add_ethic = ethic_xenophobe
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_militarist } }
#							    pop_add_ethic = ethic_militarist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_pacifist } }
#							    pop_add_ethic = ethic_pacifist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_spiritualist } }
#							    pop_add_ethic = ethic_spiritualist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_materialist } }
#							    pop_add_ethic = ethic_materialist
#							}	
#    					}
#					}
#				}
#				best_tile_for_pop = {
#					build_pop = {
#    					name = buildable_robot_pop_3
#    					grown = yes
#    					effect = {
#    						if = {
#							    limit = { root = { has_ethic = ethic_fanatic_xenophile } }
#							    pop_add_ethic = ethic_fanatic_xenophile
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_collectivist } }
#							    pop_add_ethic = ethic_fanatic_collectivist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_individualist } }
#							    pop_add_ethic = ethic_fanatic_individualist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_xenophobe } }
#							    pop_add_ethic = ethic_fanatic_xenophobe
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_militarist } }
#							    pop_add_ethic = ethic_fanatic_militarist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_pacifist } }
#							    pop_add_ethic = ethic_fanatic_pacifist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_spiritualist } }
#							    pop_add_ethic = ethic_fanatic_spiritualist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_fanatic_materialist } }
#							    pop_add_ethic = ethic_fanatic_materialist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_xenophile } }
#							    pop_add_ethic = ethic_xenophile
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_collectivist } }
#							    pop_add_ethic = ethic_collectivist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_individualist } }
#							    pop_add_ethic = ethic_individualist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_xenophobe } }
#							    pop_add_ethic = ethic_xenophobe
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_militarist } }
#							    pop_add_ethic = ethic_militarist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_pacifist } }
#							    pop_add_ethic = ethic_pacifist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_spiritualist } }
#							    pop_add_ethic = ethic_spiritualist
#							}
#							if = {
#							    limit = { root = { has_ethic = ethic_materialist } }
#							    pop_add_ethic = ethic_materialist
#							}	
#    					}
#					}
#				}
#			}
#		}
#	}
#
#	# They're on their own (the droids colonize a planet outside the borders if available or "just disappear")
#	option = {
#		name = anomaly.189.b
#		#custom_tooltip = anomaly.189.b.tooltip
#		
#		trigger = {
#			NOT = { has_policy_flag = ai_outlawed }
#			any_system = {
#				NOR = {
#					exists = space_owner
#					has_star_flag = hostile_system
#					has_star_flag = guardian
#					has_star_flag = enclave
#				}
#				any_planet = {
#					habitable_planet = yes 
#					has_anomaly = no
#					is_colony = no
#					is_capital = no
#				}
#			}
#		}
#		allow = {
#			NOT = { has_policy_flag = ai_outlawed }
#			any_system = {
#				NOR = {
#					exists = space_owner
#					has_star_flag = hostile_system
#					has_star_flag = guardian
#					has_star_flag = enclave
#				}
#				any_planet = {
#					habitable_planet = yes 
#					has_anomaly = no
#					is_colony = no
#					is_capital = no
#				}
#			}	
#		}
#		hidden_effect = {
#			country_event = { id = anomaly.1197 }
#		}
#	}
#
#	#Failsafe option: dismantle the robots for parts
#	option = {
#		name = anomaly.189.c
#		custom_tooltip = anomaly.186.a.alt.tooltip
#		add_minerals = 1500
#	}
#}
#
## continuation of option anomaly.189.b
#country_event = {
#	id = anomaly.1197
#	hide_window = yes
#
#	is_triggered_only = yes
#
#	immediate = {
#		if = {
#			limit = {
#				any_system = {
#					NOR = {
#						exists = space_owner
#						has_star_flag = hostile_system
#						has_star_flag = guardian
#						has_star_flag = enclave
#						has_star_flag = primitive_system
#					}
#					any_planet = {
#						habitable_planet = yes 
#						has_anomaly = no
#						is_colony = no
#						is_capital = no
#						NOR = {
#							exists = owner
#							exists = controller
#						}
#					}
#				}
#			}
#			else = {
#				# There are no colonizable planets outside the player's borders
#				country_event = { id = anomaly.1195 days = 40 }
#			}
#			random_system = { #This is an effect and it needs a limit.
#				limit = {
#					NOR = {
#						exists = space_owner
#						has_star_flag = hostile_system
#						has_star_flag = guardian
#						has_star_flag = enclave
#						has_star_flag = primitive_system
#					}
#					any_planet = {
#						habitable_planet = yes
#						has_anomaly = no
#						is_colony = no
#						is_capital = no
#						NOR = {
#							exists = owner
#							exists = controller
#						}
#					}
#				}
#		
#				set_star_flag = mirandas_system
#	
#				random_system_planet = {
#					limit = {
#						habitable_planet = yes
#						has_owner = no
#						is_capital = no
#					}
#	
#					save_event_target_as = colonized_planet
#					random_tile = {
#						limit = { has_deposit = no }
#						add_deposit = d_immense_mineral_deposit
#					}
#					best_tile_for_pop = {
#						build_pop = {
#    						name = buildable_robot_pop_3
#    						grown = yes
#    					}
#    					last_created_pop = {
#    						save_event_target_as = og_bot
#    					}
#    				}	
#	
#    				surveyed = {
#    					set_surveyed = yes
#    					surveyor = root
#    				}
#
#    				prevent_anomaly = yes 
#					if = {
#						limit = {
#							root = {
#								has_technology = tech_droid_workers
#								NOT = { has_technology = tech_synthetic_workers }
#							}
#						}
#						create_country = {
#							name = "Awoken"
#							name_list = FUN3
#							species = ROBOT_POP_SPECIES_2
#							type = default
#							ethos = random
#							government = random
#							auto_delete = no
#							flag = {
#								icon = {
#									category = "blocky"
#									file = "flag_blocky_21.dds"
#								}
#								background = {
#									category = "backgrounds"
#									file = "new_dawn.dds"
#								}
#								colors = {
#									"black"
#									"dark_blue"
#									"null"
#									"null"
#								}
#							}
#							day_zero_contact = no
#							effect = {
#								establish_communications_no_message = root
#							}		
#						}
#						else = {
#							create_country = {
#								name = "Awoken"
#								name_list = FUN3
#								species = ROBOT_POP_SPECIES_2
#								type = default
#								ethos = random
#								government = random
#								auto_delete = no
#								flag = {
#									icon = {
#										category = "blocky"
#										file = "flag_blocky_21.dds"
#									}
#									background = {
#										category = "backgrounds"
#										file = "new_dawn.dds"
#									}
#									colors = {
#										"black"
#										"dark_blue"
#										"null"
#										"null"
#									}
#								}
#								day_zero_contact = no
#								effect = {
#									establish_communications_no_message = root
#								}		
#							}
#						}
#					}
#
#					set_owner = last_created_country
#					set_capital = yes
#					create_colony = {
#						owner = last_created_country
#						species = owner_main_species
#						ethos = owner
#					}
#					
#					if = { 
#						limit = {
#							root = {
#								has_technology = tech_droid_workers
#								NOT = { has_technology = tech_synthetic_workers }
#							}
#						}
#						while = {
#							count = 3
#							best_tile_for_pop = {
#								build_pop = {
#    								name = buildable_robot_pop_2
#    								grown = yes	
#    							}
#    						}
#						}
#						else = {
#							while = {
#								count = 3
#								best_tile_for_pop = {
#									build_pop = {
#    									name = buildable_robot_pop_3
#    									grown = yes	
#    								}
#    							}
#							}
#						}
#					}
#					
#					random_tile = {
#						limit = { 
#							has_blocker = no 
#							has_building = no
#							not = { has_deposit = d_immense_mineral_deposit }
#						}
#						set_building = "building_power_plant_1"					
#					}
#					
#					create_army = {
#						name = "Awoken Army"
#						owner = event_target:awoken_country
#						species = event_target:awoken_country
#						type = "defense_army"
#					}
#					event_target:og_bot = { kill_pop = yes }
#					root = { country_event = { id = anomaly.1196 days = 3 } }
#				}
#			}
#		}
#	}
#}
#
#country_event = {
#	id = anomaly.1190
#	title = "anomaly.1190.name"
#	desc = "anomaly.1190.desc"
#	show_sound = event_mystic_reveal
#	picture = GFX_evt_robot_assembly_plant
#	location = from
#
#	is_triggered_only = yes
#
#	option = {
#		name = anomaly.1190.a
#		add_monthly_resource_mult = {
#			resource = engineering_research
#			value = @tier3researchreward
#			min = @tier3researchmin
#			max = @tier3researchmax
#		}
#		add_monthly_resource_mult = {
#			resource = society_research
#			value = @tier3researchreward
#			min = @tier3researchmin
#			max = @tier3researchmax
#		}
#	}
#}
#
## Triggers event chain when those who previously didn't have the robot tech gets it.
#country_event = {
#	id = anomaly.1191
#	hide_window = yes
#
#	is_triggered_only = yes
#
#	trigger = {
#		last_increased_tech = "tech_robotic_workers"
#		has_country_flag = limbo_downloaded
#		NOT = { has_policy_flag = ai_outlawed }
#	}
#	immediate = { country_event = { id = anomaly.1192 days = 20 random = 20 } }
#}
#
## enabling research project to download neural patterns into robots. 
#country_event = {
#	id = anomaly.1192
#	title = "anomaly.1192.name"
#	desc = "anomaly.1192.desc"
#	show_sound = event_mystic_reveal
#	picture = GFX_evt_robot_assembly_plant
#	location = capital_scope
#
#	is_triggered_only = yes
#
#	option = {
#		name = anomaly.1192.a
#		trigger = {
#			NOT = { has_policy_flag = ai_outlawed }
#		}
#		capital_scope = {
#			enable_special_project = {
#				name = "LIMBO_2_PROJECT"
#				location = this
#				owner = root
#			}
#		}
#	}
#	#the players wait for better tech.
#	option = {
#		name = "anomaly.1192.b"
#		trigger = {
#			has_technology = tech_robotic_workers
#			NOR = {
#				has_technology = tech_droid_workers
#				has_technology = tech_synthetic_workers
#				has_policy_flag = ai_outlawed
#			}
#		}
#	}
#	# The players choose not to download the neural patterns, ending the chain.
#	option = {
#		name = anomaly.186.a
#		custom_tooltip = anomaly.186.a.alt.tooltip
#		add_monthly_resource_mult = {
#			resource = engineering_research
#			value = @tier4researchreward
#			min = @tier4researchmin
#			max = @tier4researchmax
#		}
#		hidden_effect = { remove_country_flag = limbo_downloaded }
#	}
#}
#
## Triggers event chain when players who previously didn't have the synth/droid tech gets it.
#
#country_event = {
#	id = anomaly.1193
#	hide_window = yes
#
#	is_triggered_only = yes
#	
#	trigger = {
#		last_increased_tech = "tech_droid_workers"
#		has_country_flag = limbo_downloaded
#		NOT = {	has_policy_flag = ai_outlawed }
#	}
#	
#	immediate = { country_event = { id = anomaly.1194 days = 20 random = 20 } }
#}
#
#country_event = {
#	id = anomaly.1194
#	title = "anomaly.1194.name"
#	desc = "anomaly.1194.desc"
#	show_sound = event_mystic_reveal
#	picture = GFX_evt_robot_assembly_plant
#	location = capital_scope
#
#	is_triggered_only = yes
#
#	option = {
#		name = anomaly.1194.a
#		trigger = {
#			NOT = { has_policy_flag = ai_outlawed }
#		}
#		capital_scope = {
#			enable_special_project = {
#				name = "LIMBO_2_PROJECT"
#				location = this
#				owner = root
#			}	
#		}					
#	}
#
#	# Ends the event chain
#	option = {
#		name = anomaly.186.a
#		custom_tooltip = anomaly.186.a.alt.tooltip
#		add_monthly_resource_mult = {
#			resource = engineering_research
#			value = @tier4researchreward
#			min = @tier4researchmin
#			max = @tier4researchmax
#		}
#		hidden_effect = { remove_country_flag = limbo_downloaded }
#	}
#}
#
## Notification: The synths/droids are gone without a trace. Ends the event chain.
#country_event = {
#	id = anomaly.1195
#	title = "anomaly.1195.name"
#	desc = "anomaly.1195.desc"
#	show_sound = event_mystic_reveal
#	picture = GFX_evt_sentient_AI
#	location = capital_scope
#
#	is_triggered_only = yes
#
#	option = {
#		name = OK
#	}
#}
#
## Notification: the synths/droids have colonized a planet
#country_event = {
#	id = anomaly.1196
#	title = "anomaly.1196.name"
#	desc = "anomaly.1196.desc"
#	show_sound = event_mystic_reveal
#	picture = GFX_evt_robot_assembly_plant
#	location = event_target:colonized_planet
#
#	is_triggered_only = yes
#
#	option = {
#		name = OK
#	}
#}
#
## If the players have droid or synth tech and colonizable planets inside but not outside their borders the robots offer to colonize a planet inside borders, and the players can reject them upon which they leave without a trace.
#country_event = {
#	id = anomaly.1198
#	title = "anomaly.1198.name"
#	desc = "anomaly.1198.desc"
#	show_sound = event_mystic_reveal
#	picture = GFX_evt_robot_assembly_plant
#	location = event_target:potential_target
#
#	is_triggered_only = yes
#
#	immediate = {
#		if = {
#			limit = {
#				any_planet_within_border = {
#					habitable_planet = yes
#					is_colony = no 
#					has_anomaly = no
#					is_capital = no
#					NOR = {
#						exists = owner
#						exists = controller
#					}
#				}
#			}
#			random_planet_within_border = {
#				limit = {
#					habitable_planet = yes
#					is_colony = no	
#					has_anomaly = no
#					is_capital = no
#					NOR = {
#						exists = owner
#						exists = controller
#					}
#				}
#				save_event_target_as = potential_target
#			}
#			else = {
#				capital_scope = {
#					save_event_target_as = potential_target
#				}
#			}
#		}
#	}
#
#	option = {
#		name = anomaly.189.a
#		#custom_tooltip = anomaly.189.a.tooltip
#		trigger = {
#			any_planet_within_border = {
#				habitable_planet = yes
#				is_colony = no 
#				has_anomaly = no
#				is_capital = no
#				NOR = {
#					exists = owner
#					exists = controller
#				}
#			}
#			NOT = {
#				has_policy_flag = ai_outlawed
#			}
#		}
#		allow = {
#			NOT = {
#				has_policy_flag = ai_outlawed
#			}
#			any_planet_within_border = {
#				habitable_planet = yes
#				is_colony = no 
#				has_anomaly = no
#				is_capital = no
#				NOR = {
#					exists = owner
#					exists = controller
#				}
#			}
#		}
#		hidden_effect = {
#			if = {
#				limit = {
#					capital_scope = {
#						is_same_value = event_target:potential_target
#					}
#				}
#				random_planet_within_border = {
#					limit = {
#						habitable_planet = yes
#						is_colony = no	
#						has_anomaly = no
#						is_capital = no
#						NOR = {
#							exists = owner
#							exists = controller
#						}
#					}
#					save_event_target_as = potential_target
#				}
#			}
#			event_target:potential_target = {
#				set_owner = root
#				set_controller = root
#				prevent_anomaly = yes
#				
#				best_tile_for_pop = {
#					set_building = "building_colony_shelter"
#				}
#
#				if = {
#					limit = {
#						root = {
#							has_technology = tech_droid_workers
#							NOT = { has_technology = tech_synthetic_workers }
#						}
#					}
#					while = {
#						count = 2
#						best_tile_for_pop = {
#							build_pop = {
#    							name = buildable_robot_pop_2
#    							grown = yes
#    							effect = {
#    								if = {
#									    limit = { root = { has_ethic = ethic_fanatic_xenophile } }
#									    pop_add_ethic = ethic_fanatic_xenophile
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_fanatic_collectivist } }
#									    pop_add_ethic = ethic_fanatic_collectivist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_fanatic_individualist } }
#									    pop_add_ethic = ethic_fanatic_individualist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_fanatic_xenophobe } }
#									    pop_add_ethic = ethic_fanatic_xenophobe
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_fanatic_militarist } }
#									    pop_add_ethic = ethic_fanatic_militarist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_fanatic_pacifist } }
#									    pop_add_ethic = ethic_fanatic_pacifist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_fanatic_spiritualist } }
#									    pop_add_ethic = ethic_fanatic_spiritualist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_fanatic_materialist } }
#									    pop_add_ethic = ethic_fanatic_materialist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_xenophile } }
#									    pop_add_ethic = ethic_xenophile
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_collectivist } }
#									    pop_add_ethic = ethic_collectivist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_individualist } }
#									    pop_add_ethic = ethic_individualist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_xenophobe } }
#									    pop_add_ethic = ethic_xenophobe
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_militarist } }
#									    pop_add_ethic = ethic_militarist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_pacifist } }
#									    pop_add_ethic = ethic_pacifist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_spiritualist } }
#									    pop_add_ethic = ethic_spiritualist
#									}
#									if = {
#									    limit = { root = { has_ethic = ethic_materialist } }
#									    pop_add_ethic = ethic_materialist
#									}	
#    							}
#							}
#						}
#					}
#					else = {
#						while = {
#							count = 2
#							best_tile_for_pop = {
#								build_pop = {
#    								name = buildable_robot_pop_3
#    								grown = yes
#    								effect = {
#    									if = {
#										    limit = { root = { has_ethic = ethic_fanatic_xenophile } }
#										    pop_add_ethic = ethic_fanatic_xenophile
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_fanatic_collectivist } }
#										    pop_add_ethic = ethic_fanatic_collectivist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_fanatic_individualist } }
#										    pop_add_ethic = ethic_fanatic_individualist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_fanatic_xenophobe } }
#										    pop_add_ethic = ethic_fanatic_xenophobe
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_fanatic_militarist } }
#										    pop_add_ethic = ethic_fanatic_militarist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_fanatic_pacifist } }
#										    pop_add_ethic = ethic_fanatic_pacifist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_fanatic_spiritualist } }
#										    pop_add_ethic = ethic_fanatic_spiritualist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_fanatic_materialist } }
#										    pop_add_ethic = ethic_fanatic_materialist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_xenophile } }
#										    pop_add_ethic = ethic_xenophile
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_collectivist } }
#										    pop_add_ethic = ethic_collectivist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_individualist } }
#										    pop_add_ethic = ethic_individualist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_xenophobe } }
#										    pop_add_ethic = ethic_xenophobe
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_militarist } }
#										    pop_add_ethic = ethic_militarist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_pacifist } }
#										    pop_add_ethic = ethic_pacifist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_spiritualist } }
#										    pop_add_ethic = ethic_spiritualist
#										}
#										if = {
#										    limit = { root = { has_ethic = ethic_materialist } }
#										    pop_add_ethic = ethic_materialist
#										}
#    								}
#								}
#							}
#						}
#					}
#				}
#			}
#		}
#	}
#
#	option = {
#		name = anomaly.1198.a
#		hidden_effect = { country_event = { id = anomaly.1195 days = 3 } }
#	}
#
#	#Failsafe option: dismantle the robots for parts
#	option = {
#		name = anomaly.189.c
#		custom_tooltip = anomaly.186.a.alt.tooltip
#		add_minerals = 1500
#	}
#}
#
## If the players have droid or synth tech and colonizable planets outside but not inside their borders they tell the players they are eternally grateful but they'd like to strike out on their own and rebuild their empire again.
#country_event = {
#	id = anomaly.1199
#	title = "anomaly.1199.name"
#	desc = "anomaly.1199.desc"
#	show_sound = event_mystic_reveal
#	picture = GFX_evt_robot_assembly_plant
#	location = capital_scope
#	
#	is_triggered_only = yes
#
#	#Failsafe option: dismantle the robots for parts
#	option = {
#		name = anomaly.189.c
#		custom_tooltip = anomaly.186.a.alt.tooltip
#		add_minerals = 1500
#	}
#
#	option = {
#		name = anomaly.1199.a
#		trigger = {
#			NOT = {
#				has_policy_flag = ai_outlawed
#			}
#			any_system = {
#				NOR = {
#					exists = space_owner
#					has_star_flag = hostile_system
#					has_star_flag = guardian
#					has_star_flag = enclave
#					has_star_flag = primitive_system
#				}
#				any_planet = {
#					habitable_planet = yes 
#					has_anomaly = no
#					#is_colony = no
#					#is_capital = no
#					NOR = {
#						exists = owner
#						exists = controller
#					}
#				}
#			}
#		}
#		allow = {
#			NOT = {
#				has_policy_flag = ai_outlawed
#			}
#			any_system = {
#				NOR = {
#					exists = space_owner
#					has_star_flag = hostile_system
#					has_star_flag = guardian
#					has_star_flag = enclave
#					has_star_flag = primitive_system
#				}
#				any_planet = {
#					habitable_planet = yes 
#					has_anomaly = no
#					#is_colony = no
#					#is_capital = no
#					NOR = {
#						exists = owner
#						exists = controller
#					}
#				}
#			}
#		}	
#		hidden_effect = {
#			random_system = { #This is an effect and it needs a limit.
#				limit = {
#					NOR = {
#						exists = space_owner
#						has_star_flag = hostile_system
#						has_star_flag = guardian
#						has_star_flag = enclave
#						has_star_flag = primitive_system
#					}
#					any_planet = {
#						habitable_planet = yes
#						has_anomaly = no
#						is_colony = no
#						is_capital = no
#						NOR = {
#							exists = owner
#							exists = controller
#						}
#					}
#				}
#	
#				random_system_planet = {
#					limit = {
#						habitable_planet = yes
#						has_owner = no	
#						has_anomaly = no
#						is_colony = no
#						is_capital = no
#					}
#	
#					save_event_target_as = colonized_planet
#					random_tile = {
#						limit = { has_deposit = no }
#						add_deposit = d_immense_mineral_deposit
#					}
#	
#					best_tile_for_pop = {
#						if = {
#							limit = {
#								root = { NOT = { has_technology = tech_synthetic_workers } }
#							}
#							build_pop = {
#								name = buildable_robot_pop_2
#								grown = yes
#							}
#							last_created_pop = {
#								save_event_target_as = og_bot
#							}
#	    					else = {
#	    						build_pop = {
#									name = buildable_robot_pop_3
#									grown = yes
#								}
#								last_created_pop = {
#									save_event_target_as = og_bot
#								}
#	    					}
#						}
#	    			}	
#
#	    			surveyed = {
#	    				set_surveyed = yes
#	    				surveyor = root
#	    			}	
#	    			prevent_anomaly = yes 
#	
#					if = {
#						limit = {
#							root = { NOT = { has_technology = tech_synthetic_workers } }
#						}
#						create_country = {
#							name = "Awoken"
#							name_list = FUN3
#							species = ROBOT_POP_SPECIES_2
#							type = default
#							ethos = random
#							government = random
#							auto_delete = no
#							flag = {
#								icon = {
#									category = "blocky"
#									file = "flag_blocky_21.dds"
#								}
#								background = {
#									category = "backgrounds"
#									file = "new_dawn.dds"
#								}
#								colors = {
#									"black"
#									"dark_blue"
#									"null"
#									"null"
#								}
#							}
#							day_zero_contact = no
#							effect = {
#								establish_communications_no_message = root
#								add_trust = {
#									amount = 10
#									who = root
#								}
#								save_event_target_as = awoken_country
#							}
#						}
#						else = {
#							create_country = {
#								name = "Awoken"
#								name_list = FUN3
#								species = ROBOT_POP_SPECIES_3
#								type = default
#								ethos = random
#								government = random
#								auto_delete = no
#								flag = {
#									icon = {
#										category = "blocky"
#										file = "flag_blocky_21.dds"
#									}
#									background = {
#										category = "backgrounds"
#										file = "new_dawn.dds"
#									}
#									colors = {
#										"black"
#										"dark_blue"
#										"null"
#										"null"
#									}
#								}
#								day_zero_contact = no
#								effect = {
#									establish_communications_no_message = root
#									add_trust = {
#										amount = 10
#										who = root
#									}
#									save_event_target_as = awoken_country
#								}
#							}
#						}
#					}
#					set_owner = event_target:awoken_country
#					set_capital = yes
#					create_colony = {
#						owner = event_target:awoken_country
#						species = owner_main_species
#						ethos = owner
#					}
#					if = { 
#						limit = {
#							root = {
#								NOT = { has_technology = tech_synthetic_workers }
#							}
#						}
#						while = {
#							count = 3
#							best_tile_for_pop = {
#								build_pop = {
#	    							name = buildable_robot_pop_2
#	    							grown = yes	
#	    						}
#	    					}
#						}
#						else = {
#							while = {
#								count = 3
#								best_tile_for_pop = {
#									build_pop = {
#	    								name = buildable_robot_pop_3
#	    								grown = yes	
#	    							}
#	    						}
#							}
#						}
#					}
#					
#					random_tile = {
#						limit = { 
#							has_blocker = no 
#							has_building = no
#							NOT = { has_deposit = d_immense_mineral_deposit}
#						}
#						set_building = "building_power_plant_1"				
#					}
#					
#					create_army = {
#						name = "Awoken Army"
#						owner = event_target:awoken_country
#						species = event_target:awoken_country
#						type = "defense_army"
#					}
#					event_target:og_bot = { kill_pop = yes }
#					root = { country_event = { id = anomaly.1196 days = 3 } }
#				}
#			}
#		}
#	}
#}